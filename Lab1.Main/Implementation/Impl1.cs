﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    public class Pomarańcza : Contract.IPomarańcza
    {
        public Pomarańcza() { }

        public string Napisz()
        {
            return this.GetType().ToString() + "1";
        }

        public string ZjedźMnie()
        {
            return "Mniam Mniam";
        }

        public string Wypisz()
        {
            return "Pomarańcza";
        }
    }
}
