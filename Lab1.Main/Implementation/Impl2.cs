﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    public class Jabłko : Contract.IJabłko
    {
        public Jabłko() { }

        public string Napisz()
        {
            return this.GetType().ToString() + "2";
        }

        public string ObierzMnie()
        {
            return "Ty zboczeńcu!";
        }

        public string Wypisz()
        {
            return "Jabłko";
        }
    }
}
