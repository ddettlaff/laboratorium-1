﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    public class Maszyna: Contract.IMaszyna, Contract.IPomarańcza
    {
        public string Napisz()
        {
            return "Maszyna";
        }
        
        public string ZjedźMnie()
        {
            return "Nie jadalna";
        }

        public string Włącz()
        {
            return "Działam";
        }

        public string Wypisz()
        {
            return "Działa";
        }

        string Contract.IMaszyna.Wypisz()
        {
            return "Włączona";
        }

        string Contract.IOwoc.Wypisz()
        {
            return "Kuku";
        }
    }
}
