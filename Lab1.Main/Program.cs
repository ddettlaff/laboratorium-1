﻿using System;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {
        public IList<Contract.IOwoc> Generuj()
        {
            List<Contract.IOwoc> lista = new List<Contract.IOwoc>();
            lista.Add(new Implementation.Pomarańcza());
            lista.Add(new Implementation.Jabłko());

            lista.Add(new Implementation.Pomarańcza());
            lista.Add(new Implementation.Jabłko());

            lista.Add(new Implementation.Pomarańcza());
            lista.Add(new Implementation.Jabłko());

            lista.Add(new Implementation.Pomarańcza());
            lista.Add(new Implementation.Jabłko());

            return lista;
        }

        public void Wykonaj(IList<Contract.IOwoc> lista)
        {
            foreach (var item in lista)
            {
                Console.WriteLine(item.Napisz());
            }
        }


        public static void Main(string[] args)
        {
            Program p = new Program();
            p.Wykonaj(p.Generuj());
            Implementation.Maszyna m = new Implementation.Maszyna();
            Console.WriteLine(m.Wypisz());
            Console.WriteLine((m as Contract.IPomarańcza).Wypisz());
            Console.WriteLine((m as Contract.IMaszyna).Wypisz());
            Console.ReadKey();
        }
    }
}
