﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(Contract.IOwoc);
        public static Type ISub1 = typeof(Contract.IPomarańcza);
        public static Type Impl1 = typeof(Implementation.Pomarańcza);
        
        public static Type ISub2 = typeof(Contract.IJabłko);
        public static Type Impl2 = typeof(Implementation.Jabłko);
        
        
        public static string baseMethod = "Napisz";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "ZjedźMnie";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "ObierzMnie";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Generuj";
        public static string collectionConsumerMethod = "Wykonaj";

        #endregion

        #region P3

        public static Type IOther = typeof(Contract.IMaszyna);
        public static Type Impl3 = typeof(Implementation.Maszyna);

        public static string otherCommonMethod = "Wypisz";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
